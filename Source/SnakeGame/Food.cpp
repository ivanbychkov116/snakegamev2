// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "ctime"
#include "Bonus.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	srand(time(NULL));
	MapSize = 1000;
	FoodNumForBonusSpawn = 5;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	static int32 count = FoodNumForBonusSpawn;
	if (bIsHead) 
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->ReplaceFood();
			--count;
			if (count == 0)
			{
				FVector NewLocation(rand() % MapSize, rand() % MapSize, 0);
				FTransform NewTransform(NewLocation);
				GetWorld()->SpawnActor<ABonus>(BonusActorClass, NewTransform);
				count = FoodNumForBonusSpawn;
			}
		}
	}
}

void AFood::ReplaceFood()
{
	FVector FoodLocation(rand() % MapSize, rand() % MapSize, 0);
	this->SetActorLocation(FoodLocation);
}


