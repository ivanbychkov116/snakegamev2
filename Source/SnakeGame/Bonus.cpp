// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "ctime"

// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	srand(time(NULL));

	BonusType = rand() % 2;
	BonusComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BonusComponent"));
	BonusComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BonusComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			switch (BonusType)
			{
			case 0:
				Snake->IncreaseSpeed();
				break;
			case 1:
				Snake->DecreaseSpeed();
			}
			this->Destroy();
		}
	}
}
